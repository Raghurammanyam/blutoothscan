from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_mysqldb import MySQL
from flask_marshmallow import Marshmallow
from flask_restful import Api
import os
import pymysql

app=Flask(__name__)
ma = Marshmallow(app)
db = SQLAlchemy(app)
db.init_app(app)

class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = True
    SECRET_KEY = 'Sm9obiBTY2hyb20ga2lja3MgYXNz'


class ProductionConfig(Config):
    DEBUG = False
#    print("production")


class StagingConfig(Config):
    DEVELOPMENT = True
    DEBUG = True



class DevelopmentConfig(Config):
    DEVELOPMENT = True
    DEBUG = False
    SQLALCHEMY_DATABASE_URI = "mysql://root:Welcome@123@104.199.146.29/blscan"
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False



class TestingConfig(Config):
    TESTING = True
    DEBUG = True
    #SQLALCHEMY_DATABASE_URI = 
    #QLALCHEMY_TRACK_MODIFICATIONS = False
    #QLALCHEMY_ECHO = True







config_by_name = dict(
    development=DevelopmentConfig,
    testing=TestingConfig,
    production=ProductionConfig
    )