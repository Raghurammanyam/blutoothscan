from flask import Flask

from config import app
from flask_restful import Api
import os
from controllers.check_id import scanBl
import schedule
import time
from flask_cors import CORS

# app=Flask(__name__)
app.config.from_object(os.environ['APP_SETTINGS'])
api=Api(app)
CORS(app,resources={r"/api/*":{"origins":"*"}})

from controllers.postblid import Bliddetail,delBlid
api.add_resource(Bliddetail,'/api/postblid')
api.add_resource(delBlid,'/api/deleteblid/<int:id>')
# from controllers.postid import postblscan
# api.add_resource(postblscan,'/api/postid')

# from controllers.check_id import scanBl
# api.add_resource(scanBl,'/api/get')
from controllers.getid import getBlid
api.add_resource(getBlid,'/api/getid')


if __name__=='__main__':
    scan1=scanBl()
    scan1.get()
    app.run(host='0.0.0.0')

    
  
    