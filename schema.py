from config import ma,db
from models.scanmodel import blidnumbers, roomCount
from marshmallow import fields



class blidschema(ma.ModelSchema):
    class Meta:
        model =blidnumbers
        sqla_session = db.session

class roomcountschema(ma.ModelSchema):
    class Meta:
        model = roomCount
        sqla_session = db.session