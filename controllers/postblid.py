from datetime import datetime
from flask import make_response,abort,request
from models.scanmodel import blidnumbers
from schema import blidschema
from config import db
from flask_restful import reqparse, abort, Api, Resource



class Bliddetail(Resource):
    def __init__(self):
        pass
    def post(self):
        try:
            da = request.get_json()
            print("=======",da)
            blid = da['blno']
            existing_blid = (blidnumbers.query.filter(blidnumbers.blno == blid).one_or_none())
            if existing_blid is None:
                schema = blidschema()
                new_id = schema.load(da, session=db.session).data
                db.session.add(new_id)
                db.session.commit()
                data = schema.dump(new_id).data
                return {"success":True,"data":data}
            else:
                return({"success":False,"message":"name exists already"})
        except Exception as e:
            return({"success":False,"message":str(e)})


class delBlid(Resource):
    def __init__(self):
        pass
    def delete(self,id):
        obj=db.session.query(blidnumbers).filter(blidnumbers.id==id).first()
        if obj:
            db.session.delete(obj)
            db.session.commit()
            return({"status":True,"message":"blid  deleted successfully"})
        else:
            return("No data is found on this id")