from datetime import datetime
from flask import make_response,abort,request
from models.scanmodel import blidnumbers, roomCount
from schema import blidschema,roomcountschema
from config import db
from flask_restful import reqparse, abort, Api, Resource
#from blescan import Bluetoothctl
import logging 
import sys,os
import time
import pymysql
import json
import traceback
import bluetooth
import requests
import threading
import datetime

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s - %(levelname)s - %(message)s",
    handlers=[
        logging.StreamHandler(sys.stdout)
    ])
form_logger = logging.getLogger('detailslogger')

class scanBl():
    def __init__(self):
        pass

    def get():
        try:
            threading.Timer(30.0, self.get).start()
            blid=db.session.query(blidnumbers).order_by(blidnumbers.id).all()
            if blid:
                blid_schema = blidschema(many=True)
                data = blid_schema.dump(blid).data
            
            check=[]
            for x in data:
                if bluetooth.lookup_name(x['blno'],timeout=30)!=None:
                    form_logger.info("device detected")
                    check.append(x['name'])
            if len(check)!=0:
                r = requests.post('https://maker.ifttt.com/trigger/sonoff_on/with/key/bfGwJ2QyyJcfDTWn7ga27J',verify=False)
                form_logger.info(str((check))+"members are avaliable")
                form_logger.info("Smart switch ON status is SUCCESS and status code :{}".format(r.status_code))
                existing = (roomCount.query.filter(roomCount.id == 1).one_or_none())
                if existing is None:
                    
                    da = {"count":str(';'.join(x for x in check)),"status":"ON","last_scan":str(datetime.datetime.now())}
                    schema = roomcountschema()
                    new_id = schema.load(da, session=db.session).data
                    db.session.add(new_id)
                    db.session.commit()
                    roomdetails = schema.dump(new_id).data
                else:
                    da = {"count":str(';'.join(x for x in check)),"status":"ON","last_scan":str(datetime.datetime.now())}
                    obj=db.session.query(roomCount).filter(roomCount.id==1).update(da)
                    if obj:
                        db.session.commit()
                        schema = roomcountschema()
                        count=db.session.query(roomCount).filter_by(id=1).one()
                        roomdetails = schema.dump(count).data
                
            else:
                r = requests.post('https://maker.ifttt.com/trigger/sonoff_off/with/key/bfGwJ2QyyJcfDTWn7ga27J',verify=False)
                form_logger.info("members are not avaliable")
                form_logger.info("Smart switch OFF status is SUCCESS and status code :{}".format(r.status_code))
                existing = (roomCount.query.filter(roomCount.id == 1).one_or_none())
                if existing is None:
                    da = {"count":str(0),"status":"OFF","last_scan":str(datetime.datetime.now())}
                    schema = roomcountschema()
                    new_id = schema.load(da, session=db.session).data
                    db.session.add(new_id)
                    db.session.commit()
                    roomdetails = schema.dump(new_id).data
                else:
                    da = {"count":str(0),"status":"OFF","last_scan":str(datetime.datetime.now())}
                    obj=db.session.query(roomCount).filter(roomCount.id==1).update(da)
                    if obj:
                        db.session.commit()
                        schema = roomcountschema()
                        count=db.session.query(roomCount).filter_by(id=1).one()
                        roomdetails = schema.dump(count).data


            return ({"sucess":True,"data":data,"room_details":roomdetails})
        except Exception as e:
             form_logger.warning(traceback.format_exc())
             return ({"sucess":False, "message":str(e)})



         
        

