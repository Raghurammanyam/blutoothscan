from datetime import datetime
from flask import make_response,abort,request
from models.scanmodel import blidnumbers, roomCount
from schema import blidschema,roomcountschema
from config import db
from flask_restful import reqparse, abort, Api, Resource

class getBlid(Resource):
    def __init__(self):
        pass

    def get(self):
        try:
            blid=db.session.query(blidnumbers).order_by(blidnumbers.id).all()
            if blid:
                blid_schema = blidschema(many=True)
                data = blid_schema.dump(blid).data
            schema = roomcountschema()
            count=db.session.query(roomCount).filter_by(id=1).one()
            if count:
                roomdetails = schema.dump(count).data
                print(roomdetails)
            return ({"sucess":True,"data":data,"room_details":roomdetails})
        except Exception as e:
            return({"success":False,"message":str(e)})